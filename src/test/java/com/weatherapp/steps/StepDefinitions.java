package com.weatherapp.steps;

import com.weatherapp.helpers.WeatherAppConfig;
import com.weatherapp.pages.WeatherAppHomePage;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Cucumber step definitions.
 */
public class StepDefinitions {

    /**
     * Selenium WebDriver instance.
     */
    private WebDriver driver;

    private WeatherAppHomePage homePage;


    /**
     * Open the weather app
     */
    @Given("I am on the home page")
    public void i_am_on_the_home_page() {

        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.navigate().to(WeatherAppConfig.WEATHER_CHECKER_HOME_URL);
        this.homePage = new WeatherAppHomePage(this.driver);
    }

    /**
     * Enter the postcode to the input field
     */
    @When("I enter {string} post code")
    public void i_enter_post_code(String postcode) {
        this.homePage.enterPostcode(postcode);
        this.homePage.submitPostcode();
    }
    /**
     * Validate the error message is visible.
     */
    @Then("I should see {string} message")
    public void iShouldSeeMessage(String expectedMessage) {
        this.homePage.validateErrorMessage(expectedMessage);
    }
    /**
     * Validate the weather forecast table is visible.
     */
    @Then("I should see weather forecast")
    public void i_should_see_weather_forecast() {
        this.homePage.validateWeatherWidgetIsDisplayed();
    }
    /**
     * Validate the specified property is visible..
     */
    @And("I should see {string} property")
    public void iShouldSeeProperty(String propertyName) {
        this.homePage.validateWeatherForecastHasProperty(propertyName);
    }
    /**
     * Validate the weather forecast doesn't contain properties with empty values.
     */
    @And("Weather forecast doesn't contain properties with empty values")
    public void weatherForecastDoesnTContainPropertiesWithEmptyValues() {
        this.homePage.validateWeatherForecastDoesntHavePropertiesWithEmptyValues();
    }
    /**
     * Validate time is displayed in correct format.
     */
    @And("Time is displayed in correct format")
    public void timeIsDisplayedInCorrectFormat() {
        this.homePage.validateTimeIsDisplayedInCorrectFormat();
    }

    /**
     * Close webdriver.
     */
    @After
    public void cleanUp() {
        if (this.driver != null) {
            this.driver.quit();
        }
    }


}