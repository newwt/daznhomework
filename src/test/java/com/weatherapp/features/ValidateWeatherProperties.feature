Feature: Weather forecast

  Scenario: I can always see time, temperature and humidity properties.
    
    Given I am on the home page
    When I enter "W6 0NW" post code
    Then I should see weather forecast
    And I should see "Time" property
    And I should see "Temperature" property
    And I should see "Humidity" property

  Scenario: Properties with empty values are not displayed in the weather forecast table.
    Given I am on the home page
    When I enter "W6 0NW" post code
    Then I should see weather forecast
    And Weather forecast doesn't contain properties with empty values

  Scenario: Current time should be displayed in the correct format
    Given I am on the home page
    When I enter "W6 0NW" post code
    Then I should see weather forecast
    And Time is displayed in correct format