Feature: Post code search engine

  Scenario Outline: I can enter invalid postcode and see an error message.

    Given I am on the home page
    When I enter "<postcode>" post code
    Then I should see "<error>" message

    Examples:
      | postcode | error                        |
      | B99 9AA  | Unable to find the postcode. |
      | EC1A 1BB | Invalid postcode.            |
      | N3       | Invalid postcode.            |


  Scenario Outline: I can enter a valid postcode and see the weather forecast.

    Given I am on the home page
    When I enter "<postcode>" post code
    Then I should see weather forecast

    Examples:
      | postcode |
      | W6 0NW   |
      | N3 1XP   |
