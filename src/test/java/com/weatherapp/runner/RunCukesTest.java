package com.weatherapp.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Cucumber test runner.
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/report"},
        glue = {"com.weatherapp.steps"},
        features = {"src/test/java/com/weatherapp/features"})
public class RunCukesTest {
}
