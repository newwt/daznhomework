package com.weatherapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

abstract class BasePage {

    private WebDriver driver;

    BasePage(WebDriver driver) {
        this.driver = driver;
    }


    /** Find Element on the page.
     *
     * @param cssSelector the css selector.
     * @return the WebElement.
     */
    WebElement findElement(String cssSelector){

        return this.driver.findElement(By.cssSelector(cssSelector));
    }

    WebElement waitForElement(String cssLocator) {
        WebDriverWait wait = new WebDriverWait(this.driver, 30);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssLocator)));
    }

}
