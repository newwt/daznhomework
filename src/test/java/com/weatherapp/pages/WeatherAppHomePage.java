package com.weatherapp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class WeatherAppHomePage extends BasePage {

    /**
     * Postcode field Css locator.
     */
    private final String postCodeFieldCss = ".search_3";
    /**
     * Submit button Css locator.
     */
    private final String submitButtonCss = ".submit_3";
    /**
     * Error message Css locator.
     */
    private final String errorMessageCss = "h1";
    /**
     * Weather widget css locator.
     */
    private final String weatherWidgetCss = ".tableHeader";
    /**
     * Weather data xpath.
     */
    private final String weatherDataXpath = "//tbody/tr";

    /**
     * Constructor.
     * @param driver - the Selenium WebDriver instance.
     */
    public WeatherAppHomePage(WebDriver driver) {
        super(driver);
    }
    /**
     * Enter postcode to input field.
     */
    public void enterPostcode(String postcode){

        findElement(postCodeFieldCss).sendKeys(postcode);
    }
    /**
     * Click submit.
     */
    public void submitPostcode() {

        findElement(submitButtonCss).click();
    }
    /**
     * Validate displayer error message.
     * @param expectedMessage the expected message.
     */
    public void validateErrorMessage(String expectedMessage) {
        Assert.assertEquals(expectedMessage, waitForElement(errorMessageCss).getText());
    }
    /**
     * Validate weather widget is visible.
     */
    public void validateWeatherWidgetIsDisplayed() {
        Assert.assertTrue(waitForElement(weatherWidgetCss).isDisplayed());
    }
    /**
     * Error message Css locator.
     */
    private Map<String, String> getWeatherForecastData() {
        WebElement weatherWidget = findElement(weatherWidgetCss);
        List<WebElement> allRows = weatherWidget.findElements(By.xpath(weatherDataXpath));

        Map<String, String> weatherForecastData = new HashMap<>();
        for (int i = 0; i < allRows.size(); i++) {
            weatherForecastData.put(
                    allRows.get(i).findElement(By.xpath(".//th")).getText().replace(":", ""),
                    allRows.get(i).findElement(By.xpath(".//td")).getText());
        }
        return weatherForecastData;
    }
    /**
     * Validate the weather forecast has expected property.
     */
    public void validateWeatherForecastHasProperty(String propertyName) {
        Assert.assertTrue(getWeatherForecastData().containsKey(propertyName));
    }

    /**
     * Check if weather forecast table doesn't have properties with empty values.
     */
    public void validateWeatherForecastDoesntHavePropertiesWithEmptyValues() {
        Iterator<Map.Entry<String, String>> itr = getWeatherForecastData().entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            Assert.assertFalse(entry.getKey() + " key has empty value!", entry.getValue().isEmpty());
        }
    }
    /**
     * Validate the format of displayed current time.
     */
    public void validateTimeIsDisplayedInCorrectFormat() {
        String time = getWeatherForecastData().get("Time");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD/MM/YYYY HH:mm:ss");
        try {
            simpleDateFormat.parse(time);
        } catch (ParseException e) {
            Assert.fail(String.format("Date %s is not in the correct format", time));
        }

    }
}
